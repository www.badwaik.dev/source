---
date: 2024-03-04
authors: [jayeshbadwaik]
categories:
  - GPG
---

# Preparing Yubikey for GPG

**UPDATE 2023-03-27:**
The article shows that the already generated keys are of the type `rsa4096`. However, nowadays, its
much better to use other algorithms like `ed25519` for keys. They are much more secure. `RSA`
security is only slightly more than linear while attacks grow exponentially.

---

We assume that you have a key already generated and stored in an encrypted
manner in a  secure location, such that the private key is available at
`$SECURE_KEY_PATH` and the ownertrust is available at `$OWNERTRUST_PATH`.

1. Create a directory in `tmpfs` and assign it to variable `$TMPCARD`.

    ```shell
    TMPCARD=/tmp/gpg
    mkdir -p $TMPCARD
    chmod 700 $TMPCARD # Safe Permissions for GPG
    ```

2. Import the key into the gpg instances at `$TMPCARD`

    ```shell
    gpg --homedir $TMPCARD --import $SECURE_KEY_PATH
    ```

3. Import Ownertrust for the Keys

    ```shell
    gpg --homedir $TMPCARD --import-ownertrust $OWNERTRUST_PATH
    ```

4. Check that your key is listed and trusted

    ```shell
    gpg --homedir $TMPCARD --list-keys
    gpg: checking the trustdb
    gpg: marginals needed: 3  completes needed: 1  trust model: pgp
    gpg: depth: 0  valid:   1  signed:   0  trust: 0-, 0q, 0n, 0m, 0f, 1u
    gpg: next trustdb check due at XXXX-XX-XX
    /tmp/gpg/pubring.kbx
    --------------------
    pub   rsa4096 XXXX-XX-XX [C] [expires: XXXX-XX-XX]
          XXXXXX
    uid           [ultimate] Jayesh Badwaik <email@gmail.com>
    sub   rsa4096 XXXX-XX-XX [A] [expires: XXXX-XX-XX]
    sub   rsa4096 XXXX-XX-XX [S] [expires: XXXX-XX-XX]
    sub   rsa4096 XXXX-XX-XX [E] [expires: XXXX-XX-XX]
    ```

5. Change the passphrase of the key to one for daily use.

    ```shell
    gpg --homedir $TMPCARD --edit-key XXXXXX
    gpg> passwd
    ```

6. Upload Keys to Yubikey

    ```shell
    gpg --homedir $TMPCARD --edit-key XXXXXX

    gpg> key 1
    gpg> keytocard
    ```

7. Verify that only the keystubs are present (`>` symbol in front of `ssb` is the keystub sign):

    ```shell
    → gpg --homedir $TMPCARD -K

    /tmp/gpg/pubring.kbx
    --------------------
    sec   rsa4096 XXXX-XX-XX [C] [expires: XXXX-XX-XX]
          XXXXXX
    uid           [ultimate] Jayesh Badwaik <email@gmail.com>
    ssb>  rsa4096 XXXX-XX-XX [A] [expires: XXXX-XX-XX]
    ssb>  rsa4096 XXXX-XX-XX [S] [expires: XXXX-XX-XX]
    ssb>  rsa4096 XXXX-XX-XX [E] [expires: XXXX-XX-XX]
    ```

8. Delete the `$TMPCARD` directory
