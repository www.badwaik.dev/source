---
template: home.html
---

# Jayesh Badwaik

Welcome to my personal website! I am currently a scientific researcher in the Accelerating Devices
Lab of Juelich Supercomputing Center at Forschungzentrum Juelich. My primary interests are in
computer science, physics and mathematics, with special interest in

- Computational Physics
- High Performance Computing
- Software Engineering for Large Scale Software


